#!user/bin/python
#-*- encoding:utf-8-*-
""""
@author:caoy
@copyright:sh
@time:2020/07/{DAY}
"""

from exts import db


class Role(db.Model):
    __tablename__ = "roles"
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    user_name = db.Column(db.String(15), nullable=False)
    user_age = db.Column(db.Integer, nullable=False)

    def __init__(self, name, age):
        self.user_name = name
        self.user_age = age