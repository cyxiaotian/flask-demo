#!user/bin/python
#-*- encoding:utf-8-*-
""""
@author:caoy
@copyright:sh
@time:2020/07/{DAY}
"""

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
