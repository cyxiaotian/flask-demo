from flask import Flask
from config import config
from exts import db
from models import Role
from sql_oper import add_fun


app = Flask(__name__)
app.config.from_object(config["dev-config"])

db.init_app(app)


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/add')
def add_sql():
    db.create_all()
    user = Role('caoy', 18)
    db.session.add(user)
    db.session.commit()
    return "data add success!"

@app.route('/add/<name>/<age>')
def add_data(name, age):
    add_fun(db, Role, name, age)
    return "add data operation success!"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
