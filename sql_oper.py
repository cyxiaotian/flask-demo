#!user/bin/python
#-*- encoding:utf-8-*-
""""
@author:caoy
@copyright:sh
@time:2020/07/{DAY}
"""

def add_fun(db, model, *args):
    """添加数据"""
    _ = model(*args)
    db.session.add(_)
    try:
        db.session.commit
    except db.IntegrityError:
        db.session.rollback()
    print("sql operation <add>")