#!user/bin/python
#-*- encoding:utf-8-*-
""""
@author:caoy
@copyright:sh
@time:2020/07/{DAY}
"""

import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get("SECRET_KEY") or os.urandom(25)

    @staticmethod
    def init_app(app):
        pass


class DevelopConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'data.db')
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


config = {
    'dev-config': DevelopConfig,
    'default': DevelopConfig
}
